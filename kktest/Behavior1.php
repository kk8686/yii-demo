<?php
namespace app\kktest;

/**
 * 测试行为1
 */
class Behavior1 extends \yii\base\Behavior{
	public $name = 'b1';
	
	public $age = 90;
	
	public function fb(){
		return 'fb1';
	}
	
	public function getAge2(){
		return $this->age * 2;
	}
}