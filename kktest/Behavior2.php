<?php
namespace app\kktest;

/**
 * 测试行为2
 */
class Behavior2 extends \yii\base\Behavior{
	public $name = 'b2';
	
	public $age = 80;
	
	public function fb(){
		return 'fb2';
	}
	
	public function getAge2(){
		return $this->age * 3;
	}
	
	public function say(){
		return ',我的宿主类是' . get_class($this->owner) . ',它的名字叫' . $this->owner->name;
	}
}