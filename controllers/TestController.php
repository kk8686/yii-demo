<?php
namespace app\controllers;

use Yii;

class TestController extends \yii\web\Controller
{
	/**
	 * 你自己的测试方法
	 */
	public function actionTest(){
			
	}
	
	/**
	 * 模板测试
	 */
	public function actionTestTpl(){
		return $this->render('index', [
			'name' => 'jay',
			'age' => 26,
		]);
	}
	
	/**
	 * 行为测试
	 */
    public function actionTestBehavior(){
		$result = [];
		$test = new \app\kktest\Test();
		
		$result['test name'] = $test->name; //jay
		$behavior1 = new \app\kktest\Behavior1(); //实例化一个行为
		$test->attachBehavior('bb1', $behavior1); //装载一个行为
		$result['test bb1的age'] = $test->age; //90
		$result['test bb1的fb方法'] = $test->fb(); //fb1
		$result['test 企图取bb1的name，结果是'] = $test->name; //还是jay
		$result['test bb1的age2'] = $test->age2; //180
		
		$result['test 有age属性'] = $test->hasProperty('age'); //true
		$test->detachBehavior('bb1'); //卸载bb1这个行为
		$result['test 有age属性2'] = $test->hasProperty('age'); //false
		$result['test 有fb1方法'] = $test->hasMethod('fb1'); //也是false
		
		
		$result['============'] = '分割线，懂？';
		$behavior2 = new \app\kktest\Behavior2(); //再来第2个行为
		$test->attachBehavior('bb1', $behavior2); //其实这个bb1你随便命名，卸载时用同样的名字就行
		$result['test 又bb1的age'] = $test->age; //80
		$result['test 又bb1的fb方法'] = $test->fb(); //fb2
		$result['test 又bb1的age2'] = $test->age2; //240
		$result['test 又bb1的say'] = $test->say();
		
		print_r($result);
    }
}
